const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');

const pathToClient = path.resolve(__dirname, 'src/client');

const __DEV__ = process.env.NODE_ENV === 'development';
const plugins = [
  new HtmlWebpackPlugin({
    template: pathToClient + '/index.html',
    filename: 'index.html',
    inject: 'body'
  }),
  new webpack.DefinePlugin({
    'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV),
    '__DEV__': __DEV__
  })
];

if (!__DEV__) {
  plugins.push(new webpack.optimize.UglifyJsPlugin());
}

module.exports = {
  entry: [ 'babel-polyfill', pathToClient + '/index.js' ],
  output: {
    path: path.resolve(__dirname, 'build'),
    filename: 'bundle.js'
  },
  module: {
    loaders: [
      { test: /\.js$/, loader: 'babel-loader', exclude: /node_modules/ },
      { test: /\.jsx$/, loader: 'babel-loader', exclude: /node_modules/ },
      { test: /\.scss$/,
        use: [{
          loader: 'style-loader' // creates style nodes from JS strings
        }, {
          loader: 'css-loader' // translates CSS into CommonJS
        }, {
          loader: 'sass-loader' // compiles Sass to CSS
        }]}
    ]
  },
  plugins: plugins
}
