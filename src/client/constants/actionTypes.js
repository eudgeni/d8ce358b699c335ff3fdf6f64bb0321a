// TODO: add "redux-act"

export const AUTH_INIT = 'AUTH_INIT';
export const AUTH_SUCCESS = 'AUTH_SUCCESS';
export const AUTH_ERROR = 'AUTH_ERROR';

export const LOGOUT = 'LOGOUT';
export const LOGOUT_SUCCESS = 'LOGOUT_SUCCESS';

export const TWEETS_FETCH = 'TWEETS_FETCH';
export const TWEETS_FETCH_SUCCESS = 'TWEETS_FETCH_SUCCESS';
export const TWEETS_FETCH_ERROR = 'TWEETS_FETCH_ERROR';
