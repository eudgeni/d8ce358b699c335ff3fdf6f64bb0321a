import { put, call, takeEvery, select } from 'redux-saga/effects';
import { makeRequest } from '../helpers/api';
import { fetchTweetsSuccess, fetchTweetsError } from '../actions/tweets';
import * as ActionType from '../constants/actionTypes';

function* fetchTweetsWatch() {
  const profile = yield select((state) => state.session.profile);

  if (!(profile && typeof profile.screen_name === 'string')) {
    return;
  }

  try {
    const request = yield call(makeRequest, 'GET', 'http://127.0.0.1:3000/tweets', null, { params: { screen_name: profile.screen_name } });
    yield put(fetchTweetsSuccess(request.data));
  } catch (err) {
    yield put(fetchTweetsError());
  }
}

export default function* rootSaga() {
  yield takeEvery(ActionType.TWEETS_FETCH, fetchTweetsWatch);
}
