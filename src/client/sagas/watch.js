import { fork } from 'redux-saga/effects'
import session from './session';
import tweets from './tweets';

export default function* rootSaga() {
  yield fork(session);
  yield fork(tweets);
}
