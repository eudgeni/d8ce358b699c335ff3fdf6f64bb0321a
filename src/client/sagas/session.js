import Cookies from 'js-cookie';
import { put, call, takeEvery } from 'redux-saga/effects';
import { makeRequest } from '../helpers/api';
import { loginSuccess, loginError, logoutSuccess } from '../actions/session';
import * as ActionType from '../constants/actionTypes';

const COOKIE_PATH = 'connect.sid';


function* initAuthWatch() {
  const oauthToken = Cookies.get(COOKIE_PATH);
  if (!oauthToken) return;

  try {
    const request = yield call(makeRequest, 'POST', 'http://127.0.0.1:3000/connect');
    yield put(loginSuccess(request.data.id, { name: request.data.name, screen_name: request.data.screen_name, oauthToken }));
  } catch (err) {
    Cookies.remove(COOKIE_PATH);
    yield put(loginError(err));
  }
}

function* logoutWatch() {
  const oauthToken = Cookies.get(COOKIE_PATH);
  if (!oauthToken) return;

  try {
    yield call(makeRequest, 'POST', 'http://127.0.0.1:3000/disconnect');
    Cookies.remove(COOKIE_PATH);
    yield put(logoutSuccess());
  } catch (err) {
    /* ignore */
  }
}

export default function* rootSaga() {
  yield takeEvery(ActionType.AUTH_INIT, initAuthWatch);
  yield takeEvery(ActionType.LOGOUT, logoutWatch);
}
