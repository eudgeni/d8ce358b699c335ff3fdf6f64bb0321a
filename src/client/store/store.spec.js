import { configureStore } from './store';
import { assert } from 'chai';

describe('configureStore', () => {
  let store = null;

  beforeEach(() => {
    store = configureStore();
  });

  describe('runSaga', () => {
    it('should contain a hook for `sagaMiddleware.run`', () => {
      assert.isFunction(store.runSaga);
    });
  });
});
