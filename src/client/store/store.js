import { createStore, applyMiddleware, compose } from 'redux';
import createSagaMiddleware from 'redux-saga';
import { createLogger } from 'redux-logger';

import reducers from '../reducers';
import rootSaga from '../sagas/watch';

const PRELOADED_STORE = {};
const sagaMiddleware = createSagaMiddleware();

export function configureStore () {
  let composeEnhancers = compose;
  const middleware = [
    sagaMiddleware,
  ];

  if (__DEV__) {
    if (typeof window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ === 'function') {
      composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__;
    }

    middleware.push(createLogger());
  }

  const store = createStore(
    reducers,
    PRELOADED_STORE,
    composeEnhancers(
      applyMiddleware(...middleware)
    )
  );

  store.runSaga = sagaMiddleware.run;

  store.runSaga(rootSaga);
  return store;
}

export const STORE = configureStore();
