import * as ActionType from '../constants/actionTypes';

export function initAuth() {
  return {
    type: ActionType.AUTH_INIT
  };
}

export function loginSuccess (id, profile, oauthToken) {
  return {
    type: ActionType.AUTH_SUCCESS,
    payload: {
      oauthToken: oauthToken,
      id: id,
      profile: profile
    }
  };
}

export function loginError () {
  return {
    type: ActionType.AUTH_ERROR,
  };
}

export function logout () {
  return {
    type: ActionType.LOGOUT
  }
}

export function logoutSuccess () {
  return {
    type: ActionType.LOGOUT_SUCCESS
  };
}
