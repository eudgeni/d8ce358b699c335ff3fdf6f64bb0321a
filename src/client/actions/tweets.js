import * as ActionType from '../constants/actionTypes';

export function fetchTweets () {
  return {
    type: ActionType.TWEETS_FETCH
  };
}

/**
 * @param  {Array} tweets
 */
export function fetchTweetsSuccess (tweets) {
  if (!(tweets instanceof Array)) {
    tweets = [];
  }

  return {
    type: ActionType.TWEETS_FETCH_SUCCESS,
    payload: tweets
  };
}

export function fetchTweetsError () {
  return {
    type: ActionType.TWEETS_FETCH_ERROR,
  };
}
