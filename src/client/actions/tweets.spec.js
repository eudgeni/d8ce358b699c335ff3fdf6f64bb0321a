import { assert } from 'chai';
import * as ActionType from '../constants/actionTypes';
import { fetchTweets, fetchTweetsSuccess, fetchTweetsError } from './tweets';

describe('Actions:Tweets', () => {
  it('fetchTweets', () => {
    let action = fetchTweets();
    assert.deepEqual(action, { type: ActionType.TWEETS_FETCH });

    action = fetchTweets('any');
    assert.deepEqual(action, { type: ActionType.TWEETS_FETCH });
  });

  it('fetchTweetsSuccess', () => {
    let action = fetchTweetsSuccess([]);
    assert.deepEqual(action, { type: ActionType.TWEETS_FETCH_SUCCESS, payload: [] });
    assert.isArray(action.payload);

    action = fetchTweetsSuccess({});
    assert.deepEqual(action, { type: ActionType.TWEETS_FETCH_SUCCESS, payload: [] });
    assert.isArray(action.payload);

    action = fetchTweetsSuccess(null);
    assert.deepEqual(action, { type: ActionType.TWEETS_FETCH_SUCCESS, payload: [] });
    assert.isArray(action.payload);
  });

  it('fetchTweetsError', () => {
    let action = fetchTweetsError();
    assert.deepEqual(action, { type: ActionType.TWEETS_FETCH_ERROR });

    action = fetchTweetsError('any');
    assert.deepEqual(action, { type: ActionType.TWEETS_FETCH_ERROR });
  });
});
