import { assert } from 'chai';
import { initAuth, loginSuccess, loginError, logout, logoutSuccess } from './session';
import * as ActionType from '../constants/actionTypes';

describe('Actions:Session', () => {
  it('initAuth', () => {
    let action = initAuth();
    assert.deepEqual(action, { type: ActionType.AUTH_INIT });

    action = initAuth('any');
    assert.deepEqual(action, { type: ActionType.AUTH_INIT });
  });

  it('loginSuccess', () => {
    const id = 'id';
    const profile = 'profile';
    const oauthToken = 'oauthToken';

    let action = loginSuccess(id, profile, oauthToken);

    assert.deepEqual(action, {
      type: ActionType.AUTH_SUCCESS,
      payload: { oauthToken: oauthToken, id: id, profile: profile }
    });

    action = loginSuccess(id, id, oauthToken);

    assert.notDeepEqual(action, {
      type: ActionType.AUTH_SUCCESS,
      payload: { oauthToken: oauthToken, id: id, profile: profile }
    });
  });

  it('loginError', () => {
    let action = loginError();
    assert.deepEqual(action, { type: ActionType.AUTH_ERROR });

    action = loginError(1, 2);
    assert.deepEqual(action, { type: ActionType.AUTH_ERROR });
  });

  it('logout', () => {
    let action = logout();
    assert.deepEqual(action, { type: ActionType.LOGOUT });

    action = logout(1, 2);
    assert.deepEqual(action, { type: ActionType.LOGOUT });
  });

  it('logoutSuccess', () => {
    let action = logoutSuccess();
    assert.deepEqual(action, { type: ActionType.LOGOUT_SUCCESS });

    action = logoutSuccess(1, 2);
    assert.deepEqual(action, { type: ActionType.LOGOUT_SUCCESS });
  });
});
