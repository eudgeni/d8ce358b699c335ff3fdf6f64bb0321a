import request from 'axios';

export function makeRequest(method, api, data, options) {
  return request({
    url: api,
    method: method,
    data: data,
    withCredentials: true,
    ...options
  });
}
