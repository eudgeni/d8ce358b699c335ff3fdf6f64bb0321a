import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import AppComponent from '../components/app';
import { initAuth, logout } from '../actions/session';
import { fetchTweets } from '../actions/tweets';

function mapStateToDispath (dispatch) {
  return {
    initAuth: bindActionCreators(initAuth, dispatch),
    logout: bindActionCreators(logout, dispatch),
    fetchTweets: bindActionCreators(fetchTweets, dispatch)
  };
}

export default connect(undefined, mapStateToDispath)(AppComponent);
