import { connect } from 'react-redux';

import TweerItemComponent from '../components/tweetItem';

function mapStateToProps (store, ownProps) {
  let tweet;
  if (ownProps && ownProps.id) {
    tweet = store.tweets.tweetsById[ownProps.id]
  }

  return {
    tweet: tweet
  };
}

export default connect(mapStateToProps)(TweerItemComponent);
