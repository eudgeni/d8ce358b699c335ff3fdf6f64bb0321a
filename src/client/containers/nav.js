import { connect } from 'react-redux';

import NavComponent from '../components/nav';

function mapStateToProps (store) {
  return {
    isAuthenticated: !!store.session.id,
    profile: store.session.profile
  };
}

export default connect(mapStateToProps)(NavComponent);
