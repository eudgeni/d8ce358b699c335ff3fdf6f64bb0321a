import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { fetchTweets } from '../actions/tweets';
import TweetsComponent from '../components/tweets';

function mapStateToProps (store) {
  return {
    isAuthenticated: !!store.session.id,
    tweets: store.tweets.tweets,
    tweetsLoading: store.tweets.loading
  };
}

function mapStateToDispath (dispatch) {
  return {
    fetchTweets: bindActionCreators(fetchTweets, dispatch)
  };
}

export default connect(mapStateToProps, mapStateToDispath)(TweetsComponent);
