import * as ActionType from '../constants/actionTypes';

export const initialState = {
  tweets: [],
  tweetsById: {},
  loading: false
};

export default function tweets (state = initialState, action) {
  switch (action.type) {
    case ActionType.TWEETS_FETCH:
      return {
        tweets: state.tweets,
        tweetsById: state.tweetsById,
        loading: true
      };

    case ActionType.TWEETS_FETCH_SUCCESS:
      if (!(action.payload instanceof Array)) {
        return state;
      }

      return {
        tweets: action.payload.map((item) => item.id),
        tweetsById: action.payload.reduce((acc, item) => { acc[item.id] = item; return acc; }, {}),
        loading: false
      };

    case ActionType.TWEETS_FETCH_ERROR:
    case ActionType.LOGOUT_SUCCESS:
      return {
        tweets: [],
        tweetsById: {},
        loading: false
      };

    default:
      return state;
  }
}
