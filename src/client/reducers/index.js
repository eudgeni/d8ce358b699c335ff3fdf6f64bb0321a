import { combineReducers } from 'redux';
import session from './session';
import tweets from './tweets';

const rootReducer = combineReducers({
  session,
  tweets
});

export default rootReducer;
