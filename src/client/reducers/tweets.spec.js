import { assert } from 'chai';
import { default as tweetsReducer, initialState } from './tweets';
import * as ActionType from '../constants/actionTypes';

describe('Reducers::Tweets', () => {
  it('should set initial state by default', () => {
    const action = { type: 'unknown' };

    assert.deepEqual(tweetsReducer(undefined, action), initialState);
  });

  it('TWEETS_FETCH', () => {
    const action = { type: ActionType.TWEETS_FETCH };
    let state = {
      tweets: [],
      tweetsById: {},
      loading: false
    };
    const newState = {
      tweets: [],
      tweetsById: {},
      loading: true
    };

    assert.deepEqual(tweetsReducer(state, action), newState);
    assert.deepEqual(tweetsReducer(newState, action), newState);
  });

  it('LOGOUT_SUCCESS', () => {
    const action = { type: ActionType.LOGOUT_SUCCESS };
    assert.deepEqual(tweetsReducer(undefined, action), initialState);
  });

  it('TWEETS_FETCH_ERROR', () => {
    const action = { type: ActionType.TWEETS_FETCH_ERROR };
    assert.deepEqual(tweetsReducer(undefined, action), initialState);
  });

  it('TWEETS_FETCH_SUCCESS', () => {
    let action = { type: ActionType.TWEETS_FETCH_SUCCESS, payload: [] };
    assert.deepEqual(tweetsReducer(undefined, action), initialState);

    action = { type: ActionType.TWEETS_FETCH_SUCCESS, payload: [ { id: 1 }, { id: 2 } ] };
    assert.deepEqual(tweetsReducer(undefined, action), { tweets: [ 1, 2 ], tweetsById: { 1: { id: 1 }, 2: { id: 2 }}, loading: false });


    action = { type: ActionType.TWEETS_FETCH_SUCCESS, payload: 'asd' };
    assert.deepEqual(tweetsReducer(undefined, action), initialState);
  });
});
