import { assert } from 'chai';
import { default as sessionReducer, initialState } from './session';
import * as ActionType from '../constants/actionTypes';

describe('Reducers::Session', () => {
  it('should set initial state by default', () => {
    const action = { type: 'unknown' };

    assert.deepEqual(sessionReducer(undefined, action), initialState);
  });

  it('AUTH_SUCCESS', () => {
    let payload = { id: 1, oauthToken: 2, profile: { name: 1 }};
    let action = { type: ActionType.AUTH_SUCCESS, payload: payload };
    assert.deepEqual(sessionReducer(undefined, action), payload);

    payload = { id: 1, oauthToken: 2, profile: { name: { a: 1 } }};
    action = { type: ActionType.AUTH_SUCCESS, payload: payload };
    assert.deepEqual(sessionReducer(undefined, action), payload);
  });

  it('LOGOUT_SUCCESS', () => {
    const payload = { id: 1, oauthToken: 2, profile: { name: 1 }};
    let action = { type: ActionType.LOGOUT_SUCCESS, payload: payload };
    assert.deepEqual(sessionReducer(undefined, action), initialState);

    action = { type: ActionType.LOGOUT_SUCCESS };
    assert.deepEqual(sessionReducer(payload, action), initialState);
  });
});
