import * as ActionType from '../constants/actionTypes';

export const initialState = {
  id: null,
  oauthToken: null,
  profile: null
};

export default function session (state = initialState, action) {
  switch (action.type) {
    case ActionType.AUTH_SUCCESS:
      return {
        id: action.payload.id,
        oauthToken: action.payload.oauthToken,
        profile: action.payload.profile
      };

    case ActionType.LOGOUT_SUCCESS:
      return {
        id: null,
        oauthToken: null,
        profile: null
      };

    default:
      return state;
  }
}
