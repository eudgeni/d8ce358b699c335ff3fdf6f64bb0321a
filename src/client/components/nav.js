import React from 'react';
import PropTypes from 'prop-types';
import AppBar from 'material-ui/AppBar';
import FlatButton from 'material-ui/FlatButton';
import IconButton from 'material-ui/IconButton';
import IconMenu from 'material-ui/IconMenu';
import MenuItem from 'material-ui/MenuItem';
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert';

export default class Nav extends React.Component {
  constructor() {
    super();

    this.redirectToLogIn = this.redirectToLogIn.bind(this);
    this.logOut = this.logOut.bind(this);
    this.refreshTweets = this.refreshTweets.bind(this);
  }

  renderRightElement() {
    if (!this.props.isAuthenticated)
      return this.renderLogIn();

    return this.renderLogOut();
  }

  renderLogIn() {
    return <FlatButton label='Log in' onClick={this.redirectToLogIn} />;
  }

  renderLogOut() {
    return (
      <div className='flex-align-center' style={{ color: '#fff' }}>
        {`Your name: ${this.props.profile.name}`}
        <IconMenu key={0}
          iconButtonElement={
            <IconButton iconStyle={{ color: '#fff' }}><MoreVertIcon /></IconButton>
          }
          targetOrigin={{horizontal: 'right', vertical: 'top'}}
          anchorOrigin={{horizontal: 'right', vertical: 'top'}}
        >
          <MenuItem primaryText='Refresh Tweets' onClick={this.refreshTweets} />
          <MenuItem primaryText='log out' onClick={this.logOut} />
        </IconMenu>
      </div>
    );
  }

  logOut() {
    this.props.logout();
  }

  refreshTweets () {
    this.props.fetchTweets();
  }

  redirectToLogIn() {
    window.location.href='http://127.0.0.1:3000/oauth_request';
  }

  render() {
    return (
      <div>
        <AppBar
          iconElementLeft={<span />}
          iconElementRight={this.renderRightElement()}
          title='twitter-app' />
      </div>
    )
  }
}

Nav.defaultProps = {
  isAuthenticated: false,
  profile: {}
};

Nav.propTypes = {
  isAuthenticated: PropTypes.bool,
  profile: PropTypes.object,
  logout: PropTypes.func.isRequired,
  fetchTweets: PropTypes.func.isRequired
};
