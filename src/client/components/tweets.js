import React from 'react';
import PropTypes from 'prop-types';
import TweetItemContainer from '../containers/tweetItem';
import Paper from 'material-ui/Paper';
import List from 'material-ui/List/List';
import ListItem from 'material-ui/List/ListItem';

export default class Tweets extends React.Component {
  constructor() {
    super();
    this.renderItems = this.renderItems.bind(this);
  }

  componentWillReceiveProps (nextProps) {
    if (nextProps.isAuthenticated && nextProps.isAuthenticated !== this.props.isAuthenticated) {
      this.props.fetchTweets();
    }
  }

  renderItems (tweetId) {
    return (
      <TweetItemContainer key={tweetId} id={tweetId} />
    );
  }

  renderContent() {
    if (this.props.tweetsLoading) {
      return (
        <ListItem>
          <h2>Loading ....</h2>
        </ListItem>
      );
    }

    if (this.props.tweets.length == 0)
      return;

    return (
      <List>
        {this.props.tweets.map(this.renderItems)}
      </List>
    );
  }

  render() {
    return (
      <div className='container'>
        <Paper zDepth={1}>
          {this.renderContent()}
        </Paper>
      </div>
    );
  }
}

Tweets.defaultProps = {
  isAuthenticated: false,
  tweets: []
};

Tweets.propTypes = {
  tweets: PropTypes.array.isRequired,
  isAuthenticated: PropTypes.bool.isRequired,
  fetchTweets: PropTypes.func.isRequired,
  tweetsLoading: PropTypes.bool.isRequired
};
