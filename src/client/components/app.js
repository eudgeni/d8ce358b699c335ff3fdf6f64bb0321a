import React from 'react';
import PropTypes from 'prop-types';
import NavContainer from '../containers/nav';
import TweetsContainer from '../containers/tweets';

export default class App extends React.Component {
  componentWillMount() {
    this.props.initAuth();
  }

  render () {
    return (
      <div>
        <NavContainer logout={this.props.logout} fetchTweets={this.props.fetchTweets} />
        <TweetsContainer />
      </div>
    );
  }
}

App.propTypes = {
  initAuth: PropTypes.func.isRequired,
  logout: PropTypes.func.isRequired,
  fetchTweets: PropTypes.func.isRequired
};
