import React from 'react';
import PropTypes from 'prop-types';
import ListItem from 'material-ui/List/ListItem';
import Avatar from 'material-ui/Avatar';

export default class TweetItem extends React.Component {
  render() {
    const { tweet } = this.props;
    if (!tweet.id) {
      return <span data-error='id is undefined' />;
    }

    return (
      <ListItem  leftAvatar={<Avatar src={tweet.user.profile_image_url_https} />}>
        <div><strong>{tweet.user.name}</strong> @{tweet.user.screen_name}</div>
        <br />
        <div>{tweet.text}</div>
      </ListItem>
    );
  }
}

TweetItem.defaultProps = {
  id: undefined,
  tweet: { }
};

TweetItem.propTypes = {
  tweet: PropTypes.object.isRequired,
  id: PropTypes.oneOfType([ PropTypes.number, PropTypes.string ]).isRequired
};
