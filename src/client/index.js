import React from 'react';
import ReactDOM from 'react-dom';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import { Provider } from 'react-redux';

import App from './containers/app';
import { STORE } from './store/store';

const mountElement = document.getElementById('app');
function render() {
  return (
    <Provider store={STORE}>
      <MuiThemeProvider>
        <App />
      </MuiThemeProvider>
    </Provider>
  );
}

ReactDOM.render(render(), mountElement);

import './style/index.scss';
