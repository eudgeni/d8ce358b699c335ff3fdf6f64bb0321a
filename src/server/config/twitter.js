var oauth = require('oauth');

// Get your credentials here: https://dev.twitter.com/apps
const _twitterConsumerKey = process.env.TWITTER_CONSUMER_KEY || "VsKfeuX63v6bICl2SHcXtUi9k";
const _twitterConsumerSecret = process.env.TWITTER_CONSUMER_SECRET || "3MpL3KLfeyXLEjpcrOl4v9WrEo7h4k4yGf9QgmiY144v1lUQUX";

const configOAuth = {
    requestTokenURL: 'https://twitter.com/oauth/request_token',
    accessTokenURL: 'https://twitter.com/oauth/access_token',
    callbackURL: 'http://127.0.0.1:3000/callback'
};

const consumer = new oauth.OAuth(
    configOAuth.requestTokenURL,
    configOAuth.accessTokenURL,
    _twitterConsumerKey,
    _twitterConsumerSecret,
    '1.0A',
    configOAuth.callbackURL,
    'HMAC-SHA1'
);

module.exports = consumer;
