const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const session = require('express-session');

/**
 * @param {express} app
 */
module.exports = function expressConfig (app) {
  app.set('port', process.env.PORT || 3000);

  // Add headers
  app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', 'http://127.0.0.1:' + app.get('port'));
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    res.setHeader('Access-Control-Allow-Credentials', true);

    next();
  });

  app.use(bodyParser.urlencoded({ extended: true }));
  app.use(bodyParser.json());
  app.use(cookieParser());
  app.use(session({ secret: process.env.SESSION_SECRET || "yYxpjfDKnka880eEyKx8VkmkV4RgqhS4IjVzh0Hl44Lc6", resave: false, saveUninitialized: false, cookie: { httpOnly: false } }));

  app.use(function(req, res, next) {
    res.locals.session = req.session;
    next();
  });
};
