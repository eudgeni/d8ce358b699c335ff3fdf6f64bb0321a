const twitter = require('../config/twitter');
const HttpError = require('../helpers/httpError');

function getUserInfo (req, res, next) {
  twitter.get('https://api.twitter.com/1.1/account/verify_credentials.json', req.session.oauthAccessToken, req.session.oauthAccessTokenSecret, function (error, data) {
    if (error)
      return next(error);

    res.status(200).send(data);
  });
}

function getTweetTimeLine (req, res, next) {
  if (typeof req.query.screen_name !== 'string') {
    return next(new HttpError('bad request', 400));
  }

  twitter.get(`https://api.twitter.com/1.1/statuses/user_timeline.json?screen_name=${req.query.screen_name}&count=100`, req.session.oauthAccessToken, req.session.oauthAccessTokenSecret, function (error, data) {
    if (error)
      return next(error);

    res.status(200).send(data);
  });
}

module.exports = {
  getUserInfo: getUserInfo,
  getTweetTimeLine: getTweetTimeLine
};
