const twitter = require('../config/twitter');

function sessionsConnect (req, res, next) {
  twitter.getOAuthRequestToken(function(error, oauthToken, oauthTokenSecret) {
    if (error)
      return next(error);

    req.session.oauthRequestToken = oauthToken;
    req.session.oauthRequestTokenSecret = oauthTokenSecret;
    res.redirect('https://twitter.com/oauth/authorize?oauth_token=' + req.session.oauthRequestToken);
  });
}

function sessionsCallback (req, res, next) {
  twitter.getOAuthAccessToken(req.session.oauthRequestToken, req.session.oauthRequestTokenSecret, req.query.oauth_verifier, function(error, oauthAccessToken, oauthAccessTokenSecret, data) {
    if (error)
      return next(error);

    req.session.oauthAccessToken = oauthAccessToken;
    req.session.oauthAccessTokenSecret = oauthAccessTokenSecret;
    req.session.twitterId = data.user_id

    res.redirect('/');
  });
}

function sessionsDisconnect (req, res) {
  const twitterId = req.session.twitterId;

  req.session.destroy();
  res.status(200).send({ twitterId: twitterId });
}

function sessionsCheckAuth (req, res, next) {
  if (req.session.oauthAccessToken && req.session.oauthAccessTokenSecret)
    return next();

  res.status(401).json({
    status: 'error',
    message: 'Not Authorized'
  });
}

module.exports = {
  sessionsConnect: sessionsConnect,
  sessionsCallback: sessionsCallback,
  sessionsDisconnect: sessionsDisconnect,
  sessionsCheckAuth: sessionsCheckAuth
};
