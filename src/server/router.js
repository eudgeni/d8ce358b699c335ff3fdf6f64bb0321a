const express = require('express');
const session = require('./controllers/session');
const twitter = require('./controllers/twitter');

/**
 * @param {express} app
 */
module.exports = function routerConfig (app) {
  const sessionsRouter = express.Router()
    .get('/oauth_request', session.sessionsConnect)
    .get('/callback', session.sessionsCallback)
    .post('/disconnect', session.sessionsCheckAuth, session.sessionsDisconnect);

  const indexRouter = express.Router()
    .post('/connect', session.sessionsCheckAuth, twitter.getUserInfo)
    .get('/tweets', session.sessionsCheckAuth, twitter.getTweetTimeLine);

  app.use(indexRouter);
  app.use(sessionsRouter);
};
