class HttpError extends Error {
  constructor (message, statusCode) {
    super();
    Error.captureStackTrace(this, this.constructor);

    this.name = 'HttpError';
    this.message = message;
    this.statusCode = statusCode;
  }
}

module.exports = HttpError;
