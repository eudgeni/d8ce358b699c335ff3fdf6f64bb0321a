// eslint-disable-next-line
function handleErrorPrint (err, req, res, next) {
  res.status(err.status || err.statusCode || 500);
  const response = {
    message: err.message,
    error: err
  };

  if (process.env.NODE_ENV === 'production') {
    delete response.error;
  }

  res.json(response);
}

module.exports = {
  handleErrorPrint: handleErrorPrint,
};
