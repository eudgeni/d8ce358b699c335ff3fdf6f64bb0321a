const express = require('express');
const path = require('path');
const expressConfig = require('./config/express');
const routerConfig = require('./router');
const middleware = require('./helpers/middleware');
const app = express();

expressConfig(app);
routerConfig(app);

// Serve static assets
app.use(express.static(path.resolve(__dirname, '..', '..', 'build')));

// Always return the main index.html
app.get('*', (req, res) => {
  res.sendFile(path.resolve(__dirname, '..', '..', 'build', 'index.html'));
});

// Middleware helpers
app.use(middleware.handleErrorPrint);

app.listen(app.get('port'), function appListen () {
  const separator = '--------------------------';

  console.log(separator);
  console.log('===>  Starting Server . . .');
  console.log('===>  Port: ' + app.get('port'));
  console.log('===>  Environment: ' + process.env.NODE_ENV);
  console.log(separator);
});

module.exports = app;
