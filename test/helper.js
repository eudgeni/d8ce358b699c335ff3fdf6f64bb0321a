import { JSDOM } from 'jsdom';

const jsdom = new JSDOM('<!doctype html><html><body></body></html>');
const { window } = jsdom;

function copyProps (src, target) {
    const props = Object.getOwnPropertyNames(src)
        .filter((prop) => typeof target[prop] === 'undefined')
        .map((prop) => Object.getOwnPropertyDescriptor(src, prop));

    Object.defineProperties(target, props);
}

process.env.NODE_ENV = 'test';

global.__DEV__ = false;
global.__TEST__ = true;

global.window = window;
global.document = window.document;
global.navigator = {
    userAgent: 'node.js'
};

copyProps(window, global);

global.Element = window.Element;
global.HTMLElement = window.HTMLElement;
