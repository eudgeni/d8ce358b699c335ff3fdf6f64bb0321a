# Twitter App

## Module Installation
```sh
$ npm install
```

## Set process variables
```sh
$ export TWITTER_CONSUMER_KEY=VsKfeuX63v6bICl2SHcXtUi9k
$ export TWITTER_CONSUMER_SECRET=3MpL3KLfeyXLEjpcrOl4v9WrEo7h4k4yGf9QgmiY144v1lUQUX
$ export SESSION_SECRET=yYxpjfDKnka880eEyKx8VkmkV4RgqhS4IjVzh0Hl44Lc6
```

## Running Application
```sh
# Developer mode
$ npm run start:dev

# Production mode
$ npm run start
```

# Docker way
```sh
$ docker-compose up -d
$ docker exec -it twitter-app bash
$ npm i

# Developer mode
$ npm run start:dev

# Production mode
$ npm run start
```

### Helpers scripts
```sh
# test
$ npm run test

# lint
$ npm run lint
```
